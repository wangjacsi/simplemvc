<!DOCTYPE html>
<html>

<head>
	<title>Test</title>
	<link rel="stylesheet" href="<?php echo URL; ?>public/css/default.css" />
	<link rel="stylesheet" href="http://code.jquery.com/ui/1.9.1/themes/sunny/jquery-ui.css" />	
	<script type="text/javascript" src="<?php echo URL; ?>public/js/jquery-2.0.3.min.js"></script>
	<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.9.1/jquery-ui.min.js"></script>
	<!-- Local Javascript file include-->

	<?php 
		//$this => view object
		if(isset($this->js)){
			// just check
			/*echo "<br /><br /> JS Loaded <br /><br />";
			print_r($this);
			print_r($this->js);
			echo "<br /><br /> JS Loaded <br /><br />";*/
			foreach($this->js as $js){
				//echo 456;
				echo '<script type="text/javascript" src="' . URL . 'views/' . $js . '"></script>';
			}
		}
	?>
</head>

<body>

<?php Session::init(); ?>



<div id="header">
	<!--header <br /> -->

	<?php if(Session::get('loggedIn')==false): ?>
		<a href="<?php echo URL; ?>help">Help</a>
		<a href="<?php echo URL; ?>index">Index</a>
	<?php endif; ?>
	<?php if(Session::get('loggedIn')==true): ?>
		<a href="<?php echo URL; ?>dashboard">Dashboard</a>

		<?php if(Session::get('role')=='owner'): ?>
			<a href="<?php echo URL; ?>user">Users</a>
		<?php endif; ?>

		<a href="<?php echo URL; ?>dashboard/logout">Logout</a>
	<?php else: ?>
		<a href="<?php echo URL; ?>login">Login</a>
	<?php endif; ?>
</div>

<div id="content">
