
<?php
/**
 * Simple MVC Framework - A PHP Framework (Model, View, Controller)
 *
 * @package  SimpleMVC
 * @version  1.0.0
 * @link     https://wangjacsi@bitbucket.org/wangjacsi/simplemvc.git
 * @author   Shawn Choi <wangjacsi@gmail.com>
 */

/*
|--------------------------------------------------------------------------
| Define Super Values
|--------------------------------------------------------------------------
*/
// ex) define('__PATH__', '');
define('CONFIG_PATH', 'config');

/*
|--------------------------------------------------------------------------
| Load Init Set
|--------------------------------------------------------------------------
*/
require CONFIG_PATH . 'constant.php';
require CONFIG_PATH . 'database.php';
require CONFIG_PATH . 'paths.php';

/*
|--------------------------------------------------------------------------
| Use autoload
|--------------------------------------------------------------------------
*/
require LIBS . 'autoload.php';

/*
|--------------------------------------------------------------------------
| Run Bootstrap
|--------------------------------------------------------------------------
*/
$app = new bootstrap();
//Optional set the each path
//$app->setControllerPath('c');
//$app->setModelPath('m');
//$app->setDefaultFile('crunk.php');
//$app->setErrorFile('e.php');
$app->init();

