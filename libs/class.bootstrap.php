
<?php

class bootstrap{

    private $_url = null;
    private $_controller = null;

    private $_controllerPath = 'controllers/';
    private $_modelPath = 'models/';
    private $_errorFile = 'error.php';
    private $_defaultFile = 'index.php';

    /**
     * Construct the Bootstrap
     * @return boolean|string
     */
    function __construct(){
       //echo 'sdfkdhfsdkl';
       // spl_autoload_register(array($this, '_load'));
    }

    /**
     * Starts the Bootstrap
     *
     * @return boolean
     */
    public function init(){
        //Sets the protected $_url
        $this->_getUrl();
	//print_r($this->_getUrl);
        //Load the default controller if no URL is set
        if(empty($this->_url[0])){
            $this->_loadDefaultController();
            return false;
        }

        $this->_loadExistingController();
	    $this->_callControllerMethod();
    }

    /**
     * (Optional) Set a custom path to controllers
     * @param type $path
     */
    public function setControllerPath($path){
        $this->_controllerPath = trim($path,'/') . '/';
    }
    /**
     * (Optional) Set a custom path to models
     * @param type $path
     */
    public function setModelPath($path){
        $this->_modelPath = trim($path,'/') . '/';
    }
    /**
     * (Optional) Set a custom path to error file
     * @param type $path Use the file name only, eg: error.php
     */
    public function setErrorFile($path){
        $this->_errorFile = trim($path,'/');
    }
     /**
     * (Optional) Set a custom path to default file
     * @param type $path Use the file name only, eg: index.php
     */
    public function setDefaultFile($path){
        $this->_defaultFile = trim($path,'/');
    }

    /**
     * Fetches the $_GET from 'url
     */
    private function _getUrl(){
        // URL check if there is or not
        $url = isset($_GET['url']) ? $_GET['url'] : null;
        $url = rtrim($url,'/');

        /*echo $url;
        echo '<br/>';
        echo '11111';
        die;*/
        //filter_var : Get a variable and filter it
        //FILTER_SANITIZE_URL: Remove all characters, except letters, digits and $-_.+!*'(),{}|\\^~[]`<>#%";/?:@&=
        $url = filter_var($url, FILTER_SANITIZE_URL);
        $this->_url = explode('/',$url);
        //print_r($url); //just check
        //die;
    }

    /**
     * This loads if there is no GET parameter passed
     */
    private function _loadDefaultController(){
        //require 'controllers/index.php';
        require $this->_controllerPath . $this->_defaultFile;

        //echo "sdahfasklfhal";
        //die;
        $this->_controller = new index();
        //$this->_controller->index();
        $this->_controller->loadModel('index', $this->_modelPath);
        $this->_controller->index();
    }

    /**
     * Load an existing controller if there is a GET parameter passed
     * @return boolean|string
     */
    private function _loadExistingController(){
        // check the first URL & compare with the file exist, require the php file but if not error.php run
        //$file = 'controllers/' . $this->_url[0] . '.php';
        $file = $this->_controllerPath . $this->_url[0] . '.php';

        //echo "$file";
        //die;

        if(file_exists($file)){
            require $file;
            //print_r($this->_url[0]);

            $this->_controller = new $this->_url[0];	 //makes controller object
            $this->_controller->loadModel($this->_url[0], $this->_modelPath); //makes controller's model object
        } else{
            $this->_error();
            //return false;
        }

    }
    /**
     * If a method is passed in the GET url parameter
     *
     * http://localhost/controller/method/(param)/(param)/(param)
     * url[0] = Controller
     * url[1] = Method
     * url[2] = Param
     * url[3] = Param
     * url[4] = Param
     */
    private function _callControllerMethod(){

        $length = count($this->_url);

        //make sure the method we are calling exist
        if($length > 1){
            if(!method_exists($this->_controller, $this->_url[1])){
                // URL[1] not exist!
                $this->_error();
                //return false;
            }
        }
        //Determine what to load
        switch($length) {
            case 5:
                //Controller->Method(param1, param2, param3)
                $this->_controller->{$this->_url[1]}($this->_url[2],$this->_url[3],$this->_url[4]);
                break;
            case 4:
                //Controller->Method(param1, param2)
                $this->_controller->{$this->_url[1]}($this->_url[2],$this->_url[3]);
                break;
            case 3:
                //Controller->Method(param1)
                $this->_controller->{$this->_url[1]}($this->_url[2]);
                break;
            case 2:
                //Controller->Method()
                $this->_controller->{$this->_url[1]}();
                break;
            default:
                $this->_controller->index();
                break;
        }
    }

    /**
     * Display an error page if nothing exists
     * @return boolean
     */
    private function _error(){
        //require 'controllers/error.php';
        require $this->_controllerPath . $this->_errorFile;
        $this->_controller = new Error();
        $this->_controller->index();
        exit();
        //return false;
    }

}