<?php 

class Database extends PDO{

	public function __construct($DB_TYPE, $DB_HOST, $DB_NAME, $DB_USER, $DB_PASS){
		parent::__construct($DB_TYPE.':host=' .$DB_HOST.'; dbname=' .$DB_NAME , $DB_USER, $DB_PASS);


		//parent::setAttribute(PDO::ATTR_ERRCODE, PDO::ERRMODE_EXCEPTIONS);
	}


	/**
	 * @select common
	 * @param string $sels Selected Attributions
	 * @param string $tables The name of tables to select
	 * @param string $conds The conditions of selecting
	 * @param constant $fetchMode A PDO Fetch mode
	 * @return mixed
	 * "SELECT id, role FROM user WHERE login=:login AND password=:password"
	 * 'SELECT role FROM user WHERE id= :id', array(':id' => $id));
	 * 
	 */	 
	public function selectC($sels, $tables, $conds=false, $val=array(), $fetchMode = PDO::FETCH_ASSOC){
		$sels = implode(',',$sels);
		$tables = implode(',',$tables);		
		if ($conds==false)
			$sql = "SELECT $sels FROM $tables";
		else
			$sql = "SELECT $sels FROM $tables WHERE $conds";

		$sth = $this->prepare($sql);
		foreach($val as $key => $value){
			$sth->bindValue("$key", $value);
		}
		$sth->execute();
		return $sth->fetchAll($fetchMode);
	}


	/**
	 * @select
	 * @param string $sql An SQL Query string
	 * @param string $array Parameters to bind
	 * @param constant $fetchMode A PDO Fetch mode
	 * @return mixed
	 */	 
	public function select($sql, $array = array(), $fetchMode = PDO::FETCH_ASSOC){
		$sth = $this->prepare($sql);
		foreach($array as $key => $value){
			$sth->bindValue("$key", $value);
		}
		$sth->execute();		
		return $sth->fetchAll($fetchMode);
	}


	/**
	 * @insert
	 * @param string $table A name of table to insert into
	 * @param string $data An associative array
	 * 
	 */	 
	public function insert($table, $data){
		ksort($data);
		//print_r($data);

		$fieldNames = implode('`, `', array_keys($data));
		$fieldValues = ':' . implode(', :', array_keys($data));
		
		$sth = $this->prepare("INSERT INTO $table (`$fieldNames`) VALUES ($fieldValues)");
		foreach ($data as $key => $value){
			//echo $key . "    " . $value . "<br />";
			$sth->bindValue(":$key", $value);
		}
		$sth->execute();
	}

	/**
	 * @insert
	 * @param string $table A name of table to insert into
	 * @param string $data An associative array
	 * @param string $where The Where query part 
	 */	
	public function update($table, $data, $where){
		ksort($data);
		print_r($data);

		
		$fieldDetails = null;
		foreach ($data as $key => $value){
			//$fieldDetails .= '`' . $key . '` = :' . $key. ',';
			$fieldDetails .= "`$key`=:$key,";
		}
		$fieldDetails = rtrim($fieldDetails,',');
		//print_r($fieldDetails);
		//die;
		//echo "UPDATE $table SET $fieldDetails WHERE $where";

		$sth = $this->prepare("UPDATE $table SET $fieldDetails WHERE $where");
		foreach ($data as $key => $value){
			$sth->bindValue(":$key", $value);
		}
		$sth->execute();
	}

	/**
	 * @delete
	 * @param string $table A name of table to insert into
	 * @param string $where The Where query part 
	 * @param integer $limit An limitation of number of delete
	 * @retrun mixed
	 */	
	public function delete($table,$where, $limit=1 ){
		return $this->exec("DELETE FROM $table WHERE $where LIMIT $limit");
	}


}