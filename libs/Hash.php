

<?php

class Hash {
	/**
	 * @param string $algo The algorithm (md5, sha1, whirlpool, etc)
	 * @param string $data The data to encode
	 * @param string $salt (This should be the same throughtout the system probably)
	 * @retrun string The hashed/salted data
	 * 
	 */	


	public static function create($algo, $data, $salt){
		$context = hash_init($algo, HASH_HMAC, $salt);
		hash_update($context, $data);

		return hash_final($context); 



	}

}