

<?php 


class User_Model extends Model{

	public function __construct(){
		parent::__construct();
	}

	public function userList(){
		//Method 1 select
		//return $this->db->select('SELECT id, login, role FROM user');
		
		//method 2 selectC
		$sel = array('id','login', 'role');
		$tables = array('user');
		$conds = false;		
		return $this->db->selectC($sel, $tables, $conds);
	} 

	public function userSingleList($id){
		//Method 1 select
		//return $this->db->select('SELECT id, login, role FROM user WHERE id = :id', array(':id' => $id));
		
		//method 2 selectC
		$sel = array('id','login', 'role');
		$tables = array('user');
		$conds = 'id = :id';
		return $this->db->selectC($sel, $tables, $conds,  array(':id' => $id));

		//$sth = $this->db->prepare('SELECT id, login, role FROM user WHERE id = :id');
		//$sth->execute(array('id' => $id));
		//return $sth->fetch();
	}

	public function create($data){
		$this->db->insert('user', array(
			'login' => $data['login'],
			'password' => Hash::create(HASH_TYPE, $data['password'], HASH_PASSWORD_KEY),
			'role' => $data['role']));
	}

	public function editSave($data){
		$postData = array(
			'login' => $data['login'],
			'password' => Hash::create(HASH_TYPE, $data['password'], HASH_PASSWORD_KEY),
			'role' => $data['role']);
		$this->db->update('user', $postData, "`id` = {$data['id']}");

		/*
		$sth = $this->db->prepare('UPDATE users 
			SET `login` = :login, `password` =:password, `role`=:role 
			WHERE `id`=:id');
		$sth->execute(array(
			':id' => $data['id'],
			':login' => $data['login'],
			':password' => Hash::create('md5', $data['password'], HASH_PASSWORD_KEY),
			':role' => $data['role'])
		);*/
	}

	public function delete($id){
		$result = $this->db->select('SELECT role FROM user WHERE id= :id', array(':id' => $id));
		//$sth = $this->db->prepare('SELECT role FROM user WHERE id= :id');
		//$sth->execute(array(':id' =>$id ));
		//$data = $sth->fetch();
		//print_r($data);

		//print_r($result);
		//die();

		if ($result[0]['role'] =='owner'){
			return false;
		}

		$this->db->delete('user', "id=$id");
		//$sth = $this->db->prepare('DELETE FROM user WHERE id= :id');
		//$sth->execute(array(':id' =>$id ));
	}
}